

#include <stdio.h>

#include <stdlib.h>






// read the float function. similar as scanf, returns 0 on success, -1 on EOF
int read_float(float *f, FILE* data);


// read the string function. similar as scanf, returns 0 on success, -1 on EOF
int read_string(char *s, FILE* data);

// read the integer function. similar as scanf, returns 0 on success, -1 on EOF
int read_int(int *d, FILE* data);



// open the file function, it 
int oepn_file(char *fileName, FILE **fp);

// close the file function
void close_file(FILE **in_file);