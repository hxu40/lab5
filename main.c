// arthor: Hua Xu
// date: 11-3-2021
// SP lab_1
// CS 402





#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readfile.h"
#define MAXNAME  64
#define MAX_SIZE 1024
#include <stdlib.h>
#include<conio.h>
typedef struct{
    int ID;
    char fName[MAXNAME];
    char lName[MAXNAME];
    int salary;
}Employee;

Employee emp[MAX_SIZE];

int empCount = 0;

// this function asks the user to input the Employee information, and check if they
// want to add this information to the user.
void addEmp(){
    Employee empy;
    printf("Enter the first name of the employee: ");
    scanf("%s",empy.fName);
    printf("Enter the last name of the employee: ");
    scanf("%s",empy.lName);
    printf("Enter employee's salary (30000 to 150000): ");
    scanf("%d",&empy.salary);
    // if the user salry is not correct
    while(empy.salary < 30000 || empy.salary > 150000){
        printf("Invlaid Salary\n");
        printf("Enter employee's salary (30000 to 150000): ");
        scanf("%d",&empy.salary);
    }
    int choice;
    printf("do you want to add the following employee to the DB?\n");
    printf("      %s", empy.fName);
    printf(" %s", empy.lName);
    printf(", salary: %d\n", empy.salary);
    printf("Enter 1 for yes, 0 for no: ");
    scanf("%d",&choice);
    // put the information into database
    if(choice == 1){
        empy.ID = emp[empCount-1].ID + 1;
       
        emp[empCount] = empy;
        empCount++;
    }
}

// this function require to the user to search the Employee with required last name,
// if we have the Employee with this last name, show the information of this Employee
// otherwise, show not found
void lookByLName(){
    char name[MAXNAME];
    printf("\nEnter Employee's last name (no extra spaces): ");
    scanf("%s",name);
        
    for(int i=0;i< empCount; i++){
        
        if(strcmp(emp[i].lName,name) == 0){
            printf("\n");
            printf("Fisrt_Name   Last_Name         SALARY             ID\n");
            printf("-----------------------------------------------------\n");
            printf("%s", emp[i].fName);
            printf("     \t%s", emp[i].lName);
            printf("     \t%d", emp[i].salary);
            printf("     \t%d\n", emp[i].ID);
            printf("-----------------------------------------------------\n");
            return;
        }
    }
    
    printf("Employee not found\n");
}

void lookByID(){
    int id1;
    printf("\nPlease enter employee ID: ");
    scanf("%d",&id1);
    for(int i=0;i< empCount; i++){
        // if find the employee
        if(emp[i].ID == id1){
          printf("\n");
          printf("Fisrt_Name   Last_Name         SALARY             ID\n");
          printf("-----------------------------------------------------\n");
          printf("%s", emp[i].fName);
          printf("     \t%s", emp[i].lName);
          printf("     \t%d", emp[i].salary);
          printf("     \t%d\n", emp[i].ID); 
          printf("-----------------------------------------------------\n");
          return;
        }
    }
    printf("Employee with id %d not found in DB\n", id1);
}

// this function prints out the total number of employees in the database in tabular
void printAll(){
    printf("\n");
    printf("Fisrt_Name   Last_Name         SALARY             ID\n");
    printf("-----------------------------------------------------\n");
    for(int i=0;i< empCount; i++){
    printf("%s", emp[i].fName);
    printf("     \t%s", emp[i].lName);
    printf("     \t%d", emp[i].salary);
    printf("     \t%d\n", emp[i].ID); 
    }
    printf("-----------------------------------------------------\n");
    printf("Number of Employees  (%d)\n", empCount);
}


// the format of output information
void format() {
    printf("Fisrt_Name   Last_Name    SALARY        ID\n");
    printf("-----------------------------------------------------\n");
    printf("%s", emp[1].fName);
    printf("         %s", emp[1].lName);
    printf("        %d", emp[1].salary);
    printf("        %d", emp[1].ID);
}

// this is the menu to guide the user how to handle the database
void menu() {
    printf("\nEmployee DB Menu:\n");
    printf("--------------------------\n");
    printf("(1) Print the Database\n");
    printf("(2) Lookup by ID\n");
    printf("(3) Lookup by Last Name\n");
    printf("(4) Add an Employee\n");
    printf("(5) Quit\n");
    printf("--------------------------\n");
}




// according the user input, it generate the integer. If the input is
// not correct, it will ask user re_input again
int readChoice() {
    int choice;
     printf("Enter your choice: ");
     scanf("%d",&choice); 
     while( choice > 6 || choice < 0) {
        printf("Hey, %d is not between 1 and 5, try again...", choice);
        printf("Enter your choice: ");
        scanf("%d",&choice); 
     }
     return choice;
}




int main()   
{
    char str[100];
    // ask the user to input the file name
    printf("./workersDB ");
    scanf("%s", str);
    FILE *in_file;
    int file_status;
    file_status = oepn_file(str, &in_file);
    // file can not be opened
    if (file_status == -1) {
        printf("can not open the file");
        return 0;
    }
    // read the data into array
    int ret = 0;
    while (ret != -1) {
        Employee empy;
        ret =  read_int(&empy.ID, in_file);
        ret = read_string(&*empy.fName, in_file);
        ret = read_string(&*empy.lName, in_file);
        ret = read_int(&empy.salary, in_file);
        if(ret != -1) {
        emp[empCount] = empy;
        empCount++;}
    } 
   
   // while(fscanf(in_file,"%d %s %s %df",&emp[empCount].ID, emp[empCount].fName, emp[empCount].lName, &emp[empCount].salary) != EOF){
   //     empCount++;
  //  }
    
     close_file(&in_file);
    
    int choice;
     menu();
     choice = readChoice();
       while (choice != 5){
        if(choice == 1 ) {
            printAll();
        } else if (choice == 2) {
            lookByID();
        } else if (choice == 3) {
            lookByLName();
        } else if (choice == 4) {
            addEmp();
        } 
         menu();
         choice = readChoice();
    } 

     printf("goodbye!");

    return 0;
}
